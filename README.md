# AppNotas

Commands:

# It builds Docker images from a Dockerfile searching a Dockerfile in the path ".", and tagging the image with the name thriflora

docker build -t thriflora .

# It creates a writeable container layer over the specified image (in this case a image called thriflora), and then starts it using a specified command. 

   # --rm -> Automatically remove the container when it exits
   # -i   -> Create a interactive session
   # -t   -> Emulate a terminal
   # -v ${pwd}:/thriflora -> Mounts the current working directory into the container with the name "thriflora" as a volume
   # -p 4200:4200 -> port Host port : Container Port
   # thriflora -> Image name

docker run --rm -it -v ${pwd}:/thriflora -p 4200:4200 thriflora


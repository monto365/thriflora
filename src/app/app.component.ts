import { Component, Input } from '@angular/core';
import { boolean } from 'yargs';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Thriflora';
  someCountrySelected = false
  termsAccepted = false 

  countrySelected(value:boolean){
    this.someCountrySelected = value
  }

  isTermsAcceptedChanged(){
    if (this.termsAccepted) this.termsAccepted=false
    else this.termsAccepted=true
  }
  
  btnClick() {
    alert('¡Perfecto, te avisaremos la primera!');
  }

}

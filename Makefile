.PHONY: build start

build:
	docker build -t thriflora .

start: build
	docker run --rm -it -v $$(pwd):/thriflora -p 4200:4200 thriflora

shell: build
	docker run --rm -it -v $$(pwd):/thriflora thriflora bash

test: build
	docker run --rm -it -v $$(pwd):/thriflora thriflora npm run test

test-watch: build
	docker run --rm -it -v $$(pwd):/thriflora thriflora npm run test:watch
